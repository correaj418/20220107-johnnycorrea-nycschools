package com.correa.nyc.screens.schools;

import com.correa.nyc.api.NYCApi;
import com.correa.nyc.models.School;
import com.correa.nyc.screens.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchoolsListPresenter extends BasePresenter {

    @Inject
    public SchoolsListPresenter(NYCApi api) {
        super(api);
    }

    protected void init(ViewCallback viewCallback) {
        api.schoolsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(viewCallback::onSchoolsListResponse)
                .subscribe();
    }

    public interface ViewCallback {

        void onSchoolsListResponse(List<School> schoolList);
    }
}
