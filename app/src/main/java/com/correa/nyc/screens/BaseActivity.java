package com.correa.nyc.screens;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.correa.nyc.di.NYCComponentHolder;

import javax.inject.Inject;

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity {

    @Inject
    public T presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDi();
    }

    private void initDi() {

    }
}
