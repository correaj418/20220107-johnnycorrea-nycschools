package com.correa.nyc.screens.schools;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.correa.nyc.databinding.SchoolRowBinding;
import com.correa.nyc.models.School;

import java.lang.ref.WeakReference;
import java.util.List;

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListVh> {

    private final List<School> schoolList;
    private final WeakReference<SchoolClickListener> schoolClickListenerRef;

    public SchoolsListAdapter(List<School> schoolList,
                              SchoolClickListener clickListener) {
        this.schoolList = schoolList;
        this.schoolClickListenerRef = new WeakReference<>(clickListener);
    }

    @NonNull
    @Override
    public SchoolsListVh onCreateViewHolder(@NonNull ViewGroup parent,
                                            int viewType) {
        SchoolRowBinding binding = SchoolRowBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);

        return new SchoolsListVh(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolsListVh holder,
                                 int position) {
        holder.itemView.setOnClickListener(view ->
                schoolClickListenerRef.get().onSchoolClicked(schoolList.get(position)));
        holder.binding.schoolNameTv.setText(schoolList.get(position).getSchoolName());
    }

    @Override
    public int getItemCount() {
        return schoolList.size();
    }

    public interface SchoolClickListener {

        void onSchoolClicked(School school);
    }
}

class SchoolsListVh extends RecyclerView.ViewHolder {

    SchoolRowBinding binding;

    public SchoolsListVh(@NonNull SchoolRowBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}