package com.correa.nyc.screens;

import com.correa.nyc.api.NYCApi;

import javax.inject.Inject;

public class BasePresenter {

    protected NYCApi api;

    @Inject
    public BasePresenter(NYCApi api) {
        this.api = api;
    }
}
