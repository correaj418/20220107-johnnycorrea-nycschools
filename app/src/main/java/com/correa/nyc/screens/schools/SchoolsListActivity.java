package com.correa.nyc.screens.schools;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.correa.nyc.databinding.ActivitySchoolsListBinding;
import com.correa.nyc.di.NYCComponentHolder;
import com.correa.nyc.models.School;
import com.correa.nyc.screens.BaseActivity;
import com.correa.nyc.screens.schooldetail.SchoolDetailActivity;

import java.util.List;

public class SchoolsListActivity extends BaseActivity<SchoolsListPresenter>
        implements SchoolsListPresenter.ViewCallback, SchoolsListAdapter.SchoolClickListener {

    private ActivitySchoolsListBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySchoolsListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        NYCComponentHolder.component().inject(this);

        presenter.init(this);
    }

    @Override
    public void onSchoolsListResponse(List<School> schoolList) {
        binding.schoolsRv.setLayoutManager(new LinearLayoutManager(this));
        SchoolsListAdapter schoolsAdapter = new SchoolsListAdapter(schoolList, this);
        binding.schoolsRv.setAdapter(schoolsAdapter);
        schoolsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSchoolClicked(School school) {
        startActivity(SchoolDetailActivity.getIntent(this, school));
    }
}