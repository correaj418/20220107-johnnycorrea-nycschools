package com.correa.nyc.screens.schooldetail;

import com.correa.nyc.api.NYCApi;
import com.correa.nyc.models.School;
import com.correa.nyc.screens.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchoolDetailPresenter extends BasePresenter {

    @Inject
    public SchoolDetailPresenter(NYCApi api) {
        super(api);
    }

    protected void init(SchoolDetailPresenter.ViewCallback viewCallback,
                        String schoolId) {
        api.schoolDetails(schoolId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(schools -> {
                    if (schools.isEmpty()) {
                        viewCallback.onSchoolDetailsNotFound();
                        return;
                    }
                    viewCallback.onSchoolDetailResponse(schools.get(0));
                })
                .subscribe();
    }



    public interface ViewCallback {

        void onSchoolDetailResponse(School school);

        void onSchoolDetailsNotFound();
    }
}
