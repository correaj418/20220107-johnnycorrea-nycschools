package com.correa.nyc.screens.schooldetail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.correa.nyc.R;
import com.correa.nyc.databinding.ActivitySchoolDetailBinding;
import com.correa.nyc.di.NYCComponentHolder;
import com.correa.nyc.models.School;
import com.correa.nyc.screens.BaseActivity;

public class SchoolDetailActivity extends BaseActivity<SchoolDetailPresenter>
        implements SchoolDetailPresenter.ViewCallback {

    private static final String SCHOOL_ID_KEY = "SCHOOL_ID_KEY";

    private ActivitySchoolDetailBinding binding;

    public static Intent getIntent(Context context, School school) {
        Intent intent = new Intent(context, SchoolDetailActivity.class);
        intent.putExtra(SCHOOL_ID_KEY, school.getSchoolId());

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySchoolDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        NYCComponentHolder.component().inject(this);

        presenter.init(this, getIntent().getExtras().getString(SCHOOL_ID_KEY));
    }

    @Override
    public void onSchoolDetailResponse(School school) {
        binding.schoolNameTv.setText(getString(R.string.detail_school_name, school.getSchoolName()));
        binding.numOfSatTestTakersTv.setText(getString(R.string.detail_num_sat_takers, school.getNumOfSatTestTakers()));
        binding.mathSatScoreTv.setText(getString(R.string.detail_math_score, school.getMathScoreAvg()));
        binding.readingSatScoreTv.setText(getString(R.string.detail_reading_score, school.getReadingScoreAvg()));
        binding.writingSatScoreTv.setText(getString(R.string.detail_writing_score, school.getWritingScoreAvg()));
    }

    @Override
    public void onSchoolDetailsNotFound() {
        Toast.makeText(this, R.string.detail_school_not_found, Toast.LENGTH_LONG).show();
    }
}