package com.correa.nyc.models

import com.google.gson.annotations.SerializedName

data class School(
    @SerializedName("dbn") val schoolId: String,
    @SerializedName("school_name") val schoolName: String,
    @SerializedName("num_of_sat_test_takers") val numOfSatTestTakers: String?,
    @SerializedName("sat_math_avg_score") val mathScoreAvg: String?,
    @SerializedName("sat_critical_reading_avg_score") val readingScoreAvg: String?,
    @SerializedName("sat_writing_avg_score") val writingScoreAvg: String?)