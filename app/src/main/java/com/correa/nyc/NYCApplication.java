package com.correa.nyc;

import android.app.Application;

import com.correa.nyc.di.DaggerNYCComponent;
import com.correa.nyc.di.NYCComponentHolder;

public class NYCApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        NYCComponentHolder.initialize(DaggerNYCComponent.create());
    }
}
