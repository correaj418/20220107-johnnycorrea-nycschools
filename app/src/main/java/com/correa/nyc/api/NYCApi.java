package com.correa.nyc.api;

import com.correa.nyc.models.School;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NYCApi {

    @GET("s3k6-pzi2.json")
    Single<List<School>> schoolsList();

    @GET("f9bf-2cp4.json")
    Single<List<School>> schoolDetails(@Query("dbn") String schoolId);
}
