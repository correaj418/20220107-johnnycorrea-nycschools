package com.correa.nyc.di;

import com.correa.nyc.screens.schooldetail.SchoolDetailActivity;
import com.correa.nyc.screens.schools.SchoolsListActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NYCModule.class})
public interface NYCComponent {

    void inject(SchoolsListActivity activity);
    void inject(SchoolDetailActivity activity);

    @Component.Builder
    interface Builder {

        Builder nycModule(NYCModule module);

        NYCComponent build();
    }
}
