package com.correa.nyc.di;

public class NYCComponentHolder {

    private static NYCComponent component;

    public static void initialize(NYCComponent component) {
        NYCComponentHolder.component = component;
    }

    public static NYCComponent component() {
        return component;
    }
}
