package com.correa.nyc.screens.schools;

import com.correa.nyc.NYCBaseTest;
import com.correa.nyc.api.NYCApi;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.Single;

public class SchoolsListPresenterTest extends NYCBaseTest {

    private final SchoolsListPresenter testPresenter;

    private final NYCApi mockApi;

    public SchoolsListPresenterTest() {
        mockApi = Mockito.mock(NYCApi.class);
        testPresenter = new SchoolsListPresenter(mockApi);
    }

    @Test
    public void test_init_verifyCallbackOccursOnSuccess() {
        Mockito.when(mockApi.schoolsList()).thenReturn(Single.just(Mockito.mock(List.class)));

        SchoolsListPresenter.ViewCallback mockView = Mockito.mock(
                SchoolsListPresenter.ViewCallback.class);

        testPresenter.init(mockView);

        Mockito.verify(mockView, Mockito.times(1))
                .onSchoolsListResponse(Mockito.anyList());
    }
}