package com.correa.nyc.screens.schooldetail;

import com.correa.nyc.NYCBaseTest;
import com.correa.nyc.api.NYCApi;
import com.correa.nyc.models.School;
import com.correa.nyc.screens.schools.SchoolsListPresenter;

import junit.framework.TestCase;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.Single;

public class SchoolDetailPresenterTest extends NYCBaseTest {

    private final SchoolDetailPresenter testPresenter;

    private final NYCApi mockApi;

    public SchoolDetailPresenterTest() {
        mockApi = Mockito.mock(NYCApi.class);
        testPresenter = new SchoolDetailPresenter(mockApi);
    }

    @Test
    public void test_init_foundSchool_verifyCallbackOccurs() {
        List<School> mockList = Mockito.mock(List.class);
        Mockito.when(mockList.isEmpty()).thenReturn(false);
        Mockito.when(mockApi.schoolDetails("")).thenReturn(Single.just(mockList));

        SchoolDetailPresenter.ViewCallback mockView = Mockito.mock(
                SchoolDetailPresenter.ViewCallback.class);

        testPresenter.init(mockView, "");

        Mockito.verify(mockView, Mockito.times(1))
                .onSchoolDetailResponse(Mockito.any());
    }
    @Test
    public void test_init_schoolNotFound_verifyCallbackOccurs() {
        List<School> mockList = Mockito.mock(List.class);
        Mockito.when(mockList.isEmpty()).thenReturn(true);
        Mockito.when(mockApi.schoolDetails("")).thenReturn(Single.just(mockList));

        SchoolDetailPresenter.ViewCallback mockView = Mockito.mock(
                SchoolDetailPresenter.ViewCallback.class);

        testPresenter.init(mockView, "");

        Mockito.verify(mockView, Mockito.times(1))
                .onSchoolDetailsNotFound();
    }
}